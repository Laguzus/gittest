extends Node2D

var move_speed = 200
var start_vector = Vector2(1,1)


func _process(delta):
	translate(start_vector * delta * move_speed)


func _on_Area2D_area_entered(area : Area2D) -> void: 
	var groups : Array = area.get_groups()
	if not "Collidable" in groups:
		return # Nichts zum kollidieren, raus hier
	
	assert(("Paddel" in groups) != ("Barrier" in groups), "Collidable can only be 'Paddel' OR 'Barrier'")
	#                           ^^ leider hat GDScript kein XOR, aber so funktioniert das auch...
	if "Paddel" in groups:
		start_vector.x *= -1.0
		return
	
	if "Barrier" in groups:
		start_vector.y *= -1.0
		return
	
	push_error("Collidable %s has no useful group (%s)" % [area.name, groups])

# Gefällt mir doch nicht so gut
#func _on_Area2D_area_entered(area : Area2D) -> void: 
#	# Das Signal kommt jetzt vom Ball selbst
#	# Damit muss man nicht alle Area2D mit dem Ball verbinden
#	# Was folgt ist natürlich ein Overkill, aber ein schönes Beispiel
#
#	var groups : Array = area.get_groups() # brauchen wir öfter, also in eine Variable damit!
#
#	if groups.size() != 1:
#		push_warning("Overlapping Area2D '%s' has not exactly 1 group (%d)" % [area.name, groups.size()])
#		# Jede Area2D mit der der Ball kollidieren kann sollte nur eine Gruppe haben
#
#	for group in groups: # Sollte dem nicht so sein, können wir als failsafe einfach alle durchprobieren
#		match group: # signal beenden, wenn eine brauchbare Gruppe gefunden wurde
#			"Paddel":
#				start_vector.x *= -1.0
#				return
#			"Barrier":
#				start_vector.y *= -1.0
#				return
#
#	push_warning("Overlapping Area2D '%s' has no useful groups for Ball (%s)" % [area.name, groups])
#	# Man könnte auch im verherein überprüfen ob ne Gruppe dabei ist
