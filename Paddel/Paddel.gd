extends Node2D
class_name Paddel
tool

export var upper_limit : float = 100.0
export var lower_limit : float = 500.0
export var move_speed : float = 400.0

export var move_up : String = "" setget set_move_up
func set_move_up(new : String) -> void:
	move_up = new
	update_configuration_warning()


export var move_down : String = "" setget set_move_down
func set_move_down(new : String) -> void:
	move_down = new
	update_configuration_warning()


func _get_configuration_warning() -> String:
	var ok_up : bool = ProjectSettings.has_setting("input/" + move_up)
	var ok_down : bool = ProjectSettings.has_setting("input/" + move_down)
	if ok_up and ok_down:
		return ""
	var output : String = "Invalid input-actions"
	output += "\nmove_up: %s (%s)" % [move_up, "valid" if ok_up else "invalid"]
	output += "\nmove_down: %s (%s)" % [move_down, "valid" if ok_down else "invalid"]
	return output


func _ready() -> void:
	if Engine.editor_hint: return
	assert(InputMap.has_action(move_up), "%s is no valid input" % move_up)
	assert(InputMap.has_action(move_down), "%s is no valid input" % move_down)


func _process(delta : float) -> void:
	if Engine.editor_hint: return
	var movement : float = Input.get_vector("dummy", "dummy", move_up, move_down).y
	movement *= delta * move_speed
	position.y = clamp(position.y + movement, upper_limit, lower_limit)
