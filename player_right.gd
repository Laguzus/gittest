extends Node2D

var upper_limit = 100
var lower_limit = 600 - 100
var move_speed = 400
onready var player = get_node(".")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	if Input.is_action_pressed("player_right_up"):
		if player.position.y > upper_limit:
			player.position.y -= move_speed*delta
	if Input.is_action_pressed("player_right_down"):
		if player.position.y < lower_limit:
			player.position.y += move_speed*delta
